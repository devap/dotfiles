# inspired by
# https://github.com/junegunn/fzf/wiki/examples#changing-directory

function fd --description "Pipe git diff through fzf"
  set preview "git diff $argv --color=always -- {-1}"
  git diff $@ --name-only | fzf -m --ansi --preview $preview
end
