# inspired by
# https://github.com/junegunn/fzf/wiki/examples#changing-directory

function weather --description "Get the weather"
  echo "Retrieving the weather for: $argv"
  curl http://wttr.in/$argv
end
