# wp-cli
abbr wpcc "wp core config" # Generate a wp-config.php file.
abbr wpcd "wp core download" # Download core WordPress files.
abbr wpci "wp core install" # Create the WordPress tables in the database.
abbr wpcii "wp core is-installed" # Determine if the WordPress tables are installed.
abbr wpcmc "wp core multisite-convert" # Transform a single-site install into a multi-site install.
abbr wpcmi "wp core multisite-install" # Install multisite from scratch.
abbr wpcu "wp core update" # Update WordPress.
abbr wpcudb "wp core update-db" # Update the WordPress database.
abbr wpcv "wp core version" # Display the WordPress version.

abbr wpdbcli "wp db cli" # Open a mysql console using the WordPress credentials.
abbr wpdbc "wp db create" # Create the database, as specified in wp-config.php
abbr wpdbd "wp db drop" # Delete the database.
abbr wpdbe "wp db export" # Exports the database to a file or to STDOUT.
abbr wpdbi "wp db import" # Import database from a file or from STDIN.
abbr wpdbo "wp db optimize" # Optimize the database.
abbr wpdbq "wp db query" # Execute a query against the database.
abbr wpdbrp "wp db repair" # Repair the database.
abbr wpdbrs "wp db reset" # Remove all tables from the database.
abbr wpdbt "wp db tables" # List the database tables.

abbr wppa "wp plugin activate" # Activate a plugin.
abbr wppda "wp plugin deactivate" # Deactivate a plugin.
abbr wppd "wp plugin delete" # Delete plugin files.
abbr wppg "wp plugin get" # Get a plugin.
abbr wppi "wp plugin install" # Install a plugin.
abbr wppii "wp plugin is-installed" # Check if the plugin is installed.
abbr wppl "wp plugin list" # Get a list of plugins.
abbr wppp "wp plugin path" # Get the path to a plugin or to the plugin directory.
abbr wppsearch "wp plugin search" # Search the wordpress.org plugin repository.
abbr wpps "wp plugin status" # See the status of one or all plugins.
abbr wppt "wp plugin toggle" # Toggle a plugin's activation state.
abbr wppu "wp plugin uninstall" # Uninstall a plugin.
abbr wppu "wp plugin update" # Update one or more plugins.

abbr wpta "wp theme activate" # Activate a theme.
abbr wptd "wp theme delete" # Delete a theme.
abbr wptda "wp theme disable" # Disable a theme in a multisite install.
abbr wpte "wp theme enable" # Enable a theme in a multisite install.
abbr wptg "wp theme get" # Get a theme
abbr wpti "wp theme install" # Install a theme.
abbr wptii "wp theme is-installed" # Check if the theme is installed.
abbr wptl "wp theme list" # Get a list of themes.
abbr wptm "wp theme mod" # Manage theme mods.
abbr wptp "wp theme path" # Get the path to a theme or to the theme directory.
abbr wptsearch "wp theme search" # Search the wordpress.org theme repository.
abbr wpts "wp theme status" # See the status of one or all themes.
abbr wptu "wp theme update" # Update one or more themes.
abbr wpmig "wp wpmdb migrate" # wp migrate db pro cli
# abbr wpsalt "curl 'https://api.wordpress.org/secret-key/1.1/salt/' | eval $CLIPBOARD; echo 'Keys copied to clipboard!'"

abbr wpsalt curl\ \'https://api.wordpress.org/secret-key/1.1/salt/\'\ \|\ pbcopy\ \;\ echo\ \'Keys\ copied\ to\ clipboard!\'
