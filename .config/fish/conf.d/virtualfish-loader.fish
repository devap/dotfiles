set -g VIRTUALFISH_VERSION 2.5.1
set -g VIRTUALFISH_PYTHON_EXEC /Users/devaprastha/.pyenv/versions/3.9.0/bin/python3.9
source /Users/devaprastha/.pyenv/versions/3.9.0/lib/python3.9/site-packages/virtualfish/virtual.fish
emit virtualfish_did_setup_plugins
